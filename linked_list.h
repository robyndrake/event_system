#ifndef ROBYN_LINKED_LIST_H
#define ROBYN_LINKED_LIST_H

struct _list_item {
	struct _list *prev;	//Doubly linked list
	struct _list *next;
	void *data;
};

struct _list {
	struct _list_item *head;
	struct _list_item *tail;
};

typedef struct _list List;

int initList(List *list);
int push(void *data);
void *pop(void);
int freeList(List *list);
#endif
